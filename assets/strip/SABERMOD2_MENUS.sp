VERSION 1
ID 138
REFERENCE SABERMENUS
COUNT 59
INDEX 0
{
   REFERENCE TEAM_SIZE
   TEXT_LANGUAGE1 "Team Size:"
   TEXT_LANGUAGE2 "Team Size:"
   TEXT_LANGUAGE3 "Team Size:"
   TEXT_LANGUAGE6 "Team Size:"
   TEXT_LANGUAGE8 "Team Size:"
}
INDEX 1
{
   REFERENCE FOLLOW
   TEXT_LANGUAGE1 "Follow"
   TEXT_LANGUAGE2 "Follow"
   TEXT_LANGUAGE3 "Follow"
   TEXT_LANGUAGE6 "Follow"
   TEXT_LANGUAGE8 "Follow"
}
INDEX 2
{
   REFERENCE REMOVE
   TEXT_LANGUAGE1 "Remove"
   TEXT_LANGUAGE2 "Remove"
   TEXT_LANGUAGE3 "Remove"
   TEXT_LANGUAGE6 "Remove"
   TEXT_LANGUAGE8 "Remove"
}
INDEX 3
{
   REFERENCE BEGIN_VOTE_TO_REMOVE
   TEXT_LANGUAGE1 "Begin vote to remove this player."
   TEXT_LANGUAGE2 "Begin vote to remove this player."
   TEXT_LANGUAGE3 "Begin vote to remove this player."
   TEXT_LANGUAGE6 "Begin vote to remove this player."
   TEXT_LANGUAGE8 "Begin vote to remove this player."
}
INDEX 4
{
   REFERENCE NO_KICK
   TEXT_LANGUAGE1 "No Kicks"
   TEXT_LANGUAGE2 "No Kicks"
   TEXT_LANGUAGE3 "No Kicks"
   TEXT_LANGUAGE6 "No Kicks"
   TEXT_LANGUAGE8 "No Kicks"
}
INDEX 5
{
   REFERENCE WITH_KICK
   TEXT_LANGUAGE1 "With Kicks"
   TEXT_LANGUAGE2 "With Kicks"
   TEXT_LANGUAGE3 "With Kicks"
   TEXT_LANGUAGE6 "With Kicks"
   TEXT_LANGUAGE8 "With Kicks"
}
INDEX 6
{
   REFERENCE BEGIN_VOTE_TO_NK
   TEXT_LANGUAGE1 "Begin vote to play with NK rules."
   TEXT_LANGUAGE2 "Begin vote to play with NK rules."
   TEXT_LANGUAGE3 "Begin vote to play with NK rules."
   TEXT_LANGUAGE6 "Begin vote to play with NK rules."
   TEXT_LANGUAGE8 "Begin vote to play with NK rules."
}
INDEX 7
{
   REFERENCE BEGIN_VOTE_TO_WK
   TEXT_LANGUAGE1 "Begin vote to play with WK rules."
   TEXT_LANGUAGE2 "Begin vote to play with WK rules."
   TEXT_LANGUAGE3 "Begin vote to play with WK rules."
   TEXT_LANGUAGE6 "Begin vote to play with WK rules."
   TEXT_LANGUAGE8 "Begin vote to play with WK rules."
}
INDEX 8
{
   REFERENCE FOLLOW_KILLER
   TEXT_LANGUAGE1 "Follow Killer:"
   TEXT_LANGUAGE2 "Follow Killer:"
   TEXT_LANGUAGE3 "Follow Killer:"
   TEXT_LANGUAGE6 "Follow Killer:"
   TEXT_LANGUAGE8 "Follow Killer:"
}
INDEX 9
{
   REFERENCE FOLLOW_KILLER_DESC
   TEXT_LANGUAGE1 "When player you are following dies, switch to his killer."
   TEXT_LANGUAGE2 "When player you are following dies, switch to his killer."
   TEXT_LANGUAGE3 "When player you are following dies, switch to his killer."
   TEXT_LANGUAGE6 "When player you are following dies, switch to his killer."
   TEXT_LANGUAGE8 "When player you are following dies, switch to his killer."
}
INDEX 10
{
   REFERENCE FOLLOW_POWERUP
   TEXT_LANGUAGE1 "Follow Powerup:"
   TEXT_LANGUAGE2 "Follow Powerup:"
   TEXT_LANGUAGE3 "Follow Powerup:"
   TEXT_LANGUAGE6 "Follow Powerup:"
   TEXT_LANGUAGE8 "Follow Powerup:"
}
INDEX 11
{
   REFERENCE FOLLOW_POWERUP_DESC
   TEXT_LANGUAGE1 "Automatically follow flag and powerup carriers."
   TEXT_LANGUAGE2 "Automatically follow flag and powerup carriers."
   TEXT_LANGUAGE3 "Automatically follow flag and powerup carriers."
   TEXT_LANGUAGE6 "Automatically follow flag and powerup carriers."
   TEXT_LANGUAGE8 "Automatically follow flag and powerup carriers."
}
INDEX 12
{
   REFERENCE ROUND_LIMIT
   TEXT_LANGUAGE1 "Round Limit:"
   TEXT_LANGUAGE2 "Round Limit:"
   TEXT_LANGUAGE3 "Round Limit:"
   TEXT_LANGUAGE6 "Round Limit:"
   TEXT_LANGUAGE8 "Round Limit:"
}
INDEX 13
{
   REFERENCE THE_NUMBER_OF_ROUNDS
   TEXT_LANGUAGE1 "The number of rounds a match will last."
   TEXT_LANGUAGE2 "The number of rounds a match will last."
   TEXT_LANGUAGE3 "The number of rounds a match will last."
   TEXT_LANGUAGE6 "The number of rounds a match will last."
   TEXT_LANGUAGE8 "The number of rounds a match will last."
}
INDEX 14
{
   REFERENCE RED_ROVER
   TEXT_LANGUAGE1 "Red Rover"
   TEXT_LANGUAGE2 "Red Rover"
   TEXT_LANGUAGE3 "Red Rover"
   TEXT_LANGUAGE6 "Red Rover"
   TEXT_LANGUAGE8 "Red Rover"
}
INDEX 15
{
   REFERENCE MODE
   TEXT_LANGUAGE1 "Mode"
   TEXT_LANGUAGE2 "Mode"
   TEXT_LANGUAGE3 "Mode"
   TEXT_LANGUAGE6 "Mode"
   TEXT_LANGUAGE8 "Mode"
}
INDEX 16
{
   REFERENCE MODE_INFO
   TEXT_LANGUAGE1 "Set a different game mode."
   TEXT_LANGUAGE2 "Set a different game mode."
   TEXT_LANGUAGE3 "Set a different game mode."
   TEXT_LANGUAGE6 "Set a different game mode."
   TEXT_LANGUAGE8 "Set a different game mode."
}
INDEX 17
{
   REFERENCE SET_MODE
   TEXT_LANGUAGE1 "Set Mode:"
   TEXT_LANGUAGE2 "Set Mode:"
   TEXT_LANGUAGE3 "Set Mode:"
   TEXT_LANGUAGE6 "Set Mode:"
   TEXT_LANGUAGE8 "Set Mode:"
}
INDEX 18
{
   REFERENCE SELECT_MODE
   TEXT_LANGUAGE1 "Select desired mode."
   TEXT_LANGUAGE2 "Select desired mode."
   TEXT_LANGUAGE3 "Select desired mode."
   TEXT_LANGUAGE6 "Select desired mode."
   TEXT_LANGUAGE8 "Select desired mode."
}
INDEX 19
{
   REFERENCE BEGIN_VOTE_MODE
   TEXT_LANGUAGE1 "Begin vote to change game mode."
   TEXT_LANGUAGE2 "Begin vote to change game mode."
   TEXT_LANGUAGE3 "Begin vote to change game mode."
   TEXT_LANGUAGE6 "Begin vote to change game mode."
   TEXT_LANGUAGE8 "Begin vote to change game mode."
}
INDEX 20
{
   REFERENCE CLAN_ARENA
   TEXT_LANGUAGE1 "Clan Arena"
   TEXT_LANGUAGE2 "Clan Arena"
   TEXT_LANGUAGE3 "Clan Arena"
   TEXT_LANGUAGE6 "Clan Arena"
   TEXT_LANGUAGE8 "Clan Arena"
}
INDEX 21
{
   REFERENCE PRIVATE_DUEL
   TEXT_LANGUAGE1 "Private Duel:"
   TEXT_LANGUAGE2 "Private Duel:"
   TEXT_LANGUAGE3 "Private Duel:"
   TEXT_LANGUAGE6 "Private Duel:"
   TEXT_LANGUAGE8 "Private Duel:"
}
INDEX 22
{
   REFERENCE PRIVATE_DUEL_DESC
   TEXT_LANGUAGE1 "Hide other players when duelling."
   TEXT_LANGUAGE2 "Hide other players when duelling."
   TEXT_LANGUAGE3 "Hide other players when duelling."
   TEXT_LANGUAGE6 "Hide other players when duelling."
   TEXT_LANGUAGE8 "Hide other players when duelling."
}
INDEX 23
{
   REFERENCE DUEL_GLOW
   TEXT_LANGUAGE1 "Duel Glow:"
   TEXT_LANGUAGE2 "Duel Glow:"
   TEXT_LANGUAGE3 "Duel Glow:"
   TEXT_LANGUAGE6 "Duel Glow:"
   TEXT_LANGUAGE8 "Duel Glow:"
}
INDEX 24
{
   REFERENCE DUEL_GLOW_DESC
   TEXT_LANGUAGE1 "White glow around duelling players."
   TEXT_LANGUAGE2 "White glow around duelling players."
   TEXT_LANGUAGE3 "White glow around duelling players."
   TEXT_LANGUAGE6 "White glow around duelling players."
   TEXT_LANGUAGE8 "White glow around duelling players."
}
INDEX 25
{
   REFERENCE DARKEN_CORPSES
   TEXT_LANGUAGE1 "Darken Corpses:"
   TEXT_LANGUAGE2 "Darken Corpses:"
   TEXT_LANGUAGE3 "Darken Corpses:"
   TEXT_LANGUAGE6 "Darken Corpses:"
   TEXT_LANGUAGE8 "Darken Corpses:"
}
INDEX 26
{
   REFERENCE DARKEN_CORPSES_DESC
   TEXT_LANGUAGE1 "Turn corpses and dead players dark."
   TEXT_LANGUAGE2 "Turn corpses and dead players dark."
   TEXT_LANGUAGE3 "Turn corpses and dead players dark."
   TEXT_LANGUAGE6 "Turn corpses and dead players dark."
   TEXT_LANGUAGE8 "Turn corpses and dead players dark."
}
INDEX 27
{
   REFERENCE REWARDS
   TEXT_LANGUAGE1 "Rewards:"
   TEXT_LANGUAGE2 "Rewards:"
   TEXT_LANGUAGE3 "Rewards:"
   TEXT_LANGUAGE6 "Rewards:"
   TEXT_LANGUAGE8 "Rewards:"
}
INDEX 28
{
   REFERENCE REWARDS_DESC
   TEXT_LANGUAGE1 "Download "Jedi Knight Rewards 2" from jkhub.org."
   TEXT_LANGUAGE2 "Download "Jedi Knight Rewards 2" from jkhub.org."
   TEXT_LANGUAGE3 "Download "Jedi Knight Rewards 2" from jkhub.org."
   TEXT_LANGUAGE6 "Download "Jedi Knight Rewards 2" from jkhub.org."
   TEXT_LANGUAGE8 "Download "Jedi Knight Rewards 2" from jkhub.org."
}
INDEX 29
{
   REFERENCE DAMAGE_PLUMS
   TEXT_LANGUAGE1 "Damage Plums:"
   TEXT_LANGUAGE2 "Damage Plums:"
   TEXT_LANGUAGE3 "Damage Plums:"
   TEXT_LANGUAGE6 "Damage Plums:"
   TEXT_LANGUAGE8 "Damage Plums:"
}
INDEX 30
{
   REFERENCE DAMAGE_PLUMS_DESC
   TEXT_LANGUAGE1 "In-game damage indicators."
   TEXT_LANGUAGE2 "In-game damage indicators."
   TEXT_LANGUAGE3 "In-game damage indicators."
   TEXT_LANGUAGE6 "In-game damage indicators."
   TEXT_LANGUAGE8 "In-game damage indicators."
}
INDEX 31
{
   REFERENCE TIMER
   TEXT_LANGUAGE1 "Game Timer:"
   TEXT_LANGUAGE2 "Game Timer:"
   TEXT_LANGUAGE3 "Game Timer:"
   TEXT_LANGUAGE6 "Game Timer:"
   TEXT_LANGUAGE8 "Game Timer:"
}
INDEX 32
{
   REFERENCE TIMER_DESC
   TEXT_LANGUAGE1 "Draw game timer in the top-right corner."
   TEXT_LANGUAGE2 "Draw game timer in the top-right corner."
   TEXT_LANGUAGE3 "Draw game timer in the top-right corner."
   TEXT_LANGUAGE6 "Draw game timer in the top-right corner."
   TEXT_LANGUAGE8 "Draw game timer in the top-right corner."
}
INDEX 33
{
   REFERENCE COUNT_UP
   TEXT_LANGUAGE1 "Count up"
   TEXT_LANGUAGE2 "Count up"
   TEXT_LANGUAGE3 "Count up"
   TEXT_LANGUAGE6 "Count up"
   TEXT_LANGUAGE8 "Count up"
}
INDEX 34
{
   REFERENCE COUNT_DOWN
   TEXT_LANGUAGE1 "Count down"
   TEXT_LANGUAGE2 "Count down"
   TEXT_LANGUAGE3 "Count down"
   TEXT_LANGUAGE6 "Count down"
   TEXT_LANGUAGE8 "Count down"
}
INDEX 35
{
   REFERENCE CLOCK
   TEXT_LANGUAGE1 "Real Time Clock:"
   TEXT_LANGUAGE2 "Real Time Clock:"
   TEXT_LANGUAGE3 "Real Time Clock:"
   TEXT_LANGUAGE6 "Real Time Clock:"
   TEXT_LANGUAGE8 "Real Time Clock:"
}
INDEX 36
{
   REFERENCE CLOCK_DESC
   TEXT_LANGUAGE1 "Draw local time digital clock."
   TEXT_LANGUAGE2 "Draw local time digital clock."
   TEXT_LANGUAGE3 "Draw local time digital clock."
   TEXT_LANGUAGE6 "Draw local time digital clock."
   TEXT_LANGUAGE8 "Draw local time digital clock."
}
INDEX 37
{
   REFERENCE CHAT_BEEP
   TEXT_LANGUAGE1 "Chat Beep:"
   TEXT_LANGUAGE2 "Chat Beep:"
   TEXT_LANGUAGE3 "Chat Beep:"
   TEXT_LANGUAGE6 "Chat Beep:"
   TEXT_LANGUAGE8 "Chat Beep:"
}
INDEX 38
{
   REFERENCE CHAT_BEEP_DESC
   TEXT_LANGUAGE1 "Beep sound when someone sends a message."
   TEXT_LANGUAGE2 "Beep sound when someone sends a message."
   TEXT_LANGUAGE3 "Beep sound when someone sends a message."
   TEXT_LANGUAGE6 "Beep sound when someone sends a message."
   TEXT_LANGUAGE8 "Beep sound when someone sends a message."
}
INDEX 39
{
   REFERENCE MATCH
   TEXT_LANGUAGE1 "Match"
   TEXT_LANGUAGE2 "Match"
   TEXT_LANGUAGE3 "Match"
   TEXT_LANGUAGE6 "Match"
   TEXT_LANGUAGE8 "Match"
}
INDEX 40
{
   REFERENCE MATCH_DESC
   TEXT_LANGUAGE1 "Restrict spectator chat and disable damage plums."
   TEXT_LANGUAGE2 "Restrict spectator chat and disable damage plums."
   TEXT_LANGUAGE3 "Restrict spectator chat and disable damage plums."
   TEXT_LANGUAGE6 "Restrict spectator chat and disable damage plums."
   TEXT_LANGUAGE8 "Restrict spectator chat and disable damage plums."
}
INDEX 41
{
   REFERENCE PRACTICE
   TEXT_LANGUAGE1 "Practice"
   TEXT_LANGUAGE2 "Practice"
   TEXT_LANGUAGE3 "Practice"
   TEXT_LANGUAGE6 "Practice"
   TEXT_LANGUAGE8 "Practice"
}
INDEX 42
{
   REFERENCE PRACTICE_DESC
   TEXT_LANGUAGE1 "Disable match mode restrictions."
   TEXT_LANGUAGE2 "Disable match mode restrictions."
   TEXT_LANGUAGE3 "Disable match mode restrictions."
   TEXT_LANGUAGE6 "Disable match mode restrictions."
   TEXT_LANGUAGE8 "Disable match mode restrictions."
}
INDEX 43
{
   REFERENCE MOD_OPTIONS
   TEXT_LANGUAGE1 "SaberMod"
   TEXT_LANGUAGE2 "SaberMod"
   TEXT_LANGUAGE3 "SaberMod"
   TEXT_LANGUAGE6 "SaberMod"
   TEXT_LANGUAGE8 "SaberMod"
}
INDEX 44
{
   REFERENCE MOD_OPTIONS_DESC
   TEXT_LANGUAGE1 "Configure SaberMod options."
   TEXT_LANGUAGE2 "Configure SaberMod options."
   TEXT_LANGUAGE3 "Configure SaberMod options."
   TEXT_LANGUAGE6 "Configure SaberMod options."
   TEXT_LANGUAGE8 "Configure SaberMod options."
}
INDEX 45
{
   REFERENCE RULES
   TEXT_LANGUAGE1 "Rules"
   TEXT_LANGUAGE2 "Rules"
   TEXT_LANGUAGE3 "Rules"
   TEXT_LANGUAGE6 "Rules"
   TEXT_LANGUAGE8 "Rules"
}
INDEX 46
{
   REFERENCE RULES_DESC
   TEXT_LANGUAGE1 "Change game rules."
   TEXT_LANGUAGE2 "Change game rules."
   TEXT_LANGUAGE3 "Change game rules."
   TEXT_LANGUAGE6 "Change game rules."
   TEXT_LANGUAGE8 "Change game rules."
}
INDEX 47
{
   REFERENCE TEAMSIZE
   TEXT_LANGUAGE1 "Team Size:"
   TEXT_LANGUAGE2 "Team Size:"
   TEXT_LANGUAGE3 "Team Size:"
   TEXT_LANGUAGE6 "Team Size:"
   TEXT_LANGUAGE8 "Team Size:"
}
INDEX 48
{
   REFERENCE TEAMSIZE_DESC
   TEXT_LANGUAGE1 "Maximum players in a team."
   TEXT_LANGUAGE2 "Maximum players in a team."
   TEXT_LANGUAGE3 "Maximum players in a team."
   TEXT_LANGUAGE6 "Maximum players in a team."
   TEXT_LANGUAGE8 "Maximum players in a team."
}
INDEX 49
{
   REFERENCE SHORT
   TEXT_LANGUAGE1 "Short"
   TEXT_LANGUAGE2 "Short"
   TEXT_LANGUAGE3 "Short"
   TEXT_LANGUAGE6 "Short"
   TEXT_LANGUAGE8 "Short"
}
INDEX 50
{
   REFERENCE LONG
   TEXT_LANGUAGE1 "Long"
   TEXT_LANGUAGE2 "Long"
   TEXT_LANGUAGE3 "Long"
   TEXT_LANGUAGE6 "Long"
   TEXT_LANGUAGE8 "Long"
}
INDEX 51
{
   REFERENCE DISPLAY_NAME
   TEXT_LANGUAGE1 "Display Name:"
   TEXT_LANGUAGE2 "Display Name:"
   TEXT_LANGUAGE3 "Display Name:"
   TEXT_LANGUAGE6 "Display Name:"
   TEXT_LANGUAGE8 "Display Name:"
}
INDEX 52
{
   REFERENCE DISPLAY_NAME_DESC
   TEXT_LANGUAGE1 "Choose display name style."
   TEXT_LANGUAGE2 "Choose display name style."
   TEXT_LANGUAGE3 "Choose display name style."
   TEXT_LANGUAGE6 "Choose display name style."
   TEXT_LANGUAGE8 "Choose display name style."
}
INDEX 53
{
   REFERENCE SHOW_MOTD
   TEXT_LANGUAGE1 "Show Message of the Day"
   TEXT_LANGUAGE2 "Show Message of the Day"
   TEXT_LANGUAGE3 "Show Message of the Day"
   TEXT_LANGUAGE6 "Show Message of the Day"
   TEXT_LANGUAGE8 "Show Message of the Day"
}
INDEX 54
{
   REFERENCE SHUFFLE
   TEXT_LANGUAGE1 "Shuffle"
   TEXT_LANGUAGE2 "Shuffle"
   TEXT_LANGUAGE3 "Shuffle"
   TEXT_LANGUAGE6 "Shuffle"
   TEXT_LANGUAGE8 "Shuffle"
}
INDEX 55
{
   REFERENCE SHUFFLE_DESC
   TEXT_LANGUAGE1 "Randomize teams."
   TEXT_LANGUAGE2 "Randomize teams."
   TEXT_LANGUAGE3 "Randomize teams."
   TEXT_LANGUAGE6 "Randomize teams."
   TEXT_LANGUAGE8 "Randomize teams."
}
INDEX 56
{
   REFERENCE CA_CAPS
   TEXT_LANGUAGE1 "CLAN ARENA"
   TEXT_LANGUAGE2 "CLAN ARENA"
   TEXT_LANGUAGE3 "CLAN ARENA"
   TEXT_LANGUAGE6 "CLAN ARENA"
   TEXT_LANGUAGE8 "CLAN ARENA"
}
INDEX 57
{
   REFERENCE CA_CAPS_DESC
   TEXT_LANGUAGE1 "Clan Arena game rules."
   TEXT_LANGUAGE2 "Clan Arena game rules."
   TEXT_LANGUAGE3 "Clan Arena game rules."
   TEXT_LANGUAGE6 "Clan Arena game rules."
   TEXT_LANGUAGE8 "Clan Arena game rules."
}
INDEX 58
{
   REFERENCE CLAN_ARENA_RULES
   TEXT_LANGUAGE1 "In Clan Arena (CA) there are two teams trying to eliminate each other.  Match is split into a number of rounds and whichever team manages to sweep majority of them wins.  Players who die must wait for a current round to end as observers.  When a new round commences all players start fully rejuvenated with all available items and ammunition.  If a time limit is reached before any team is eliminated, a number of surviving players and collective hp of each team is used to determine the winner.\n\nTEAM SCORING\n  Winning a round:  +1 point\n\nINDIVIDUAL SCORING\n  Eliminating a member of the enemy team:  +1 point\n  Dealing 50 damage to enemy team: +1 point\n\nThe team whose score reaches round limit first wins the game."
   TEXT_LANGUAGE2 "In Clan Arena (CA) there are two teams trying to eliminate each other.  Match is split into a number of rounds and whichever team manages to sweep majority of them wins.  Players who die must wait for a current round to end as observers.  When a new round commences all players start fully rejuvenated with all available items and ammunition.  If a time limit is reached before any team is eliminated, a number of surviving players and collective hp of each team is used to determine the winner.\n\nTEAM SCORING\n  Winning a round:  +1 point\n\nINDIVIDUAL SCORING\n  Eliminating a member of the enemy team:  +1 point\n  Dealing 50 damage to enemy team: +1 point\n\nThe team whose score reaches round limit first wins the game."
   TEXT_LANGUAGE3 "In Clan Arena (CA) there are two teams trying to eliminate each other.  Match is split into a number of rounds and whichever team manages to sweep majority of them wins.  Players who die must wait for a current round to end as observers.  When a new round commences all players start fully rejuvenated with all available items and ammunition.  If a time limit is reached before any team is eliminated, a number of surviving players and collective hp of each team is used to determine the winner.\n\nTEAM SCORING\n  Winning a round:  +1 point\n\nINDIVIDUAL SCORING\n  Eliminating a member of the enemy team:  +1 point\n  Dealing 50 damage to enemy team: +1 point\n\nThe team whose score reaches round limit first wins the game."
   TEXT_LANGUAGE6 "In Clan Arena (CA) there are two teams trying to eliminate each other.  Match is split into a number of rounds and whichever team manages to sweep majority of them wins.  Players who die must wait for a current round to end as observers.  When a new round commences all players start fully rejuvenated with all available items and ammunition.  If a time limit is reached before any team is eliminated, a number of surviving players and collective hp of each team is used to determine the winner.\n\nTEAM SCORING\n  Winning a round:  +1 point\n\nINDIVIDUAL SCORING\n  Eliminating a member of the enemy team:  +1 point\n  Dealing 50 damage to enemy team: +1 point\n\nThe team whose score reaches round limit first wins the game."
   TEXT_LANGUAGE8 "In Clan Arena (CA) there are two teams trying to eliminate each other.  Match is split into a number of rounds and whichever team manages to sweep majority of them wins.  Players who die must wait for a current round to end as observers.  When a new round commences all players start fully rejuvenated with all available items and ammunition.  If a time limit is reached before any team is eliminated, a number of surviving players and collective hp of each team is used to determine the winner.\n\nTEAM SCORING\n  Winning a round:  +1 point\n\nINDIVIDUAL SCORING\n  Eliminating a member of the enemy team:  +1 point\n  Dealing 50 damage to enemy team: +1 point\n\nThe team whose score reaches round limit first wins the game."
}
